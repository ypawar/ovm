from subprocess import Popen, PIPE
import argparse

parser = argparse.ArgumentParser(description="Add/Remove physical disk to/from VM and OVM")
parser.add_argument('command',
                    nargs='?',
                    choices=['mapdisk', 'unmapdisk', 'deletedisk', 'refresh'],
                    help='Specify task :Map Disk to VM, Unmap Disk from VM ,Delete Disk from OVM and Refresh Server')
parser.add_argument('--vmname', nargs='+', help='VM Name(s) separated by space')
parser.add_argument('--lunid', nargs='+', help='WWN ID(s) separated by space')
parser.add_argument('--slot', type=int, help='Slot number to start from in case of multiple disks')
parser.add_argument('--diskname',
                    help='Disk name to give to physical disks. _n will be appended in case of multiple disks')
# Commands
list_vm = "list vm"
list_pd = "list physicaldisk"
list_vdm = "list vmdiskmapping"
list_server = "list server"

show_pd = "show physicaldisk id={0}"
show_vm = "show vm name='{0}'"
show_vdm = "show vmdiskmapping id='{0}'"

edit_pd = "edit physicaldisk id={0} shareable=Yes name='{1}'"
create_vdm = "create vmdiskmapping slot={0} physicaldisk={1} name='{2}' on vm name='{3}'"
delete_vdm = "delete vmdiskmapping id={0}"
delete_pd = "delete physicaldisk id={0}"
refresh_server = "refresh server id={0}"


def execute(cmd):
    ssh = Popen(["ssh", "admin@localhost", "-p", "10000", cmd], stdout=PIPE, stderr=PIPE)
    ssh.wait()
    return ssh.stdout.readlines()


def parse_vdm(result):
    if "Success" in result[2]:
        data = []
        for line in result[5:-1]:
            line = line.strip()
            pid = line.split(" ")[0][3:]
            data.append(pid)
        return data
    return


def parse_pd(result):
    return parse_vdm(result)


def parse_lvm(result):
    return parse_vdm(result)


def parse_server(result):
    if "Success" in result[2]:
        data = []
        for line in result[5:-1]:
            line = line.strip()
            sid = line.split(" ")[0].split(":")[1:]
            data.append(sid)
        return data


def parse_spd(result):
    if "Success" in result[2]:
        line = result[5]
        line = line.strip()
        lunid = line.split("/")[-1]
        return lunid
    return


def parse_svm(result):
    if "Success" in result[2]:
        diskmapping = []
        for line in result[5:-1]:
            line = line.strip()
            if "VmDiskMapping" in line:
                diskmapping.append(line.split(" ")[3])
        diskmapping.reverse()
        return diskmapping
    return


def parse_svdm(result):
    if "Success" in result[2]:
        for line in result[5:-1]:
            line = line.strip()
            if "Physical Disk" in line:
                return line.split(" ")[3]
    return


def find_pd(lid):
    result = execute(list_pd)
    pdids = parse_pd(result)
    if pdids:
        for pdid in pdids:
            result = execute(show_pd.format(pdid))
            lunid = parse_spd(result)
            if lunid == lid:
                return pdid
    return


def share_pd(pdid, name):
    status = True
    data = []
    for i, pd in enumerate(pdid):
        dname = name + "_" + str(i + 1)
        result = execute(edit_pd.format(pd, dname))
        if "Success" in result[2]:
            data.append((pd, dname))
            print "Share Physical Disk:{0} with name:{1} Successfull".format(pd, dname)
        else:
            print "Share Physical Disk:{0} with name:{1} Failed".format(pd, dname)
            status = False
    if status:
        return data
    else:
        return


def map_disk_to_vm(slot, pdiskid, mapname, vmname):
    result = execute(create_vdm.format(slot, pdiskid, mapname, vmname))
    if "Success" in result[2]:
        print "Map Physical Disk:{0} to VM:{1} Successfull".format(pdiskid, vmname)
        return True
    else:
        print "Map Physical Disk:{0} to VM:{1} Failed".format(pdiskid, vmname)
        return False


def unmap_disk_from_vm(vmname, lid):
    result = execute(show_vm.format(vmname))
    diskmappingids = parse_svm(result)
    if diskmappingids:
        for diskmappingid in diskmappingids:
            result = execute(show_vdm.format(diskmappingid))
            pid = parse_svdm(result)
            if pid:
                result = execute(show_pd.format(pid))
                lunid = parse_spd(result)
                if lunid:
                    if lunid == lid:
                        result = execute(delete_vdm.format(diskmappingid))
                        if "Success" in result[2]:
                            print "Unmap LUN:{0} from VM:{1} Successfull".format(lid, vmname)
                            return pid
                        else:
                            print "Unmap LUN:{0} from VM:{1} Failed".format(lid, vmname)


def delete_disk_from_ovm(pid):
    result = execute(delete_pd.format(pid))
    if "Success" in result[2]:
        print "Delete Physical Disk {0} Successfull".format(pid)
        return True
    else:
        print "Delete Physical Disk {0} Failed".format(pid)
        return False


def mapdisk(lunids, vmnames, slot, diskname):
    print "Mapping Disk(s) Started"
    status = True
    physicaldisks = []
    result = execute(list_pd)
    pdids = parse_pd(result)
    if pdids:
        for pdid in pdids:
            result = execute(show_pd.format(pdid))
            lunid = parse_spd(result)
            if lunid in lunids:
                physicaldisks.append(pdid)
                if len(physicaldisks) == len(lunids):
                    break
        if len(physicaldisks) == len(lunids):
            print "Found all disks"
            print physicaldisks
            data = share_pd(physicaldisks, diskname)
            if data:
                for i, d in enumerate(data):
                    for vm in vmnames:
                        if not map_disk_to_vm(slot + i, d[0], d[1] + "_" + vm, vm):
                            status = False
                if status:
                    print "Map all disks Successfull"
                else:
                    print "Map all disks Failed"
            else:
                print "Map all disks Failed"
        else:
            print "Couldn't find some/all disks"
            print physicaldisks


def unmapdisk(lunids, vmnames):
    print "Unmapping Disk(s) Started"
    pids = []
    status = True
    for lunid in lunids:
        for i, vm in enumerate(vmnames):
            pid = unmap_disk_from_vm(vm, lunid)
            if pid:
                if i == 0:
                    pids.append(pid)
            else:
                status = False
    # for pid in pids:
    #     if not delete_disk_from_ovm(pid):
    #         status = False
    if status:
        print "Unmap all disks Successfull"
    else:
        print "Unmap all disks Failed"


def deletedisk(lunids):
    status = True
    physicaldisks = []
    result = execute(list_pd)
    pdids = parse_pd(result)
    if pdids:
        for pdid in pdids:
            result = execute(show_pd.format(pdid))
            lunid = parse_spd(result)
            if lunid in lunids:
                physicaldisks.append(pdid)
                if len(physicaldisks) == len(lunids):
                    break

    if len(physicaldisks) == len(lunids):
        print "Found all disks"
        print physicaldisks
        for pdid in physicaldisks:
            if not delete_disk_from_ovm(pdid):
                status = False
    if status:
        print "Delete Disks from OVM Successfull"
        return
    print "Delete Disks from OVM Failed"


def refreshserver():
    status = True
    result = execute(list_server)
    serverids = parse_server(result)
    if serverids:
        for sid in serverids:
            result = execute(refresh_server.format(sid))
            if "Success" in result[2]:
                print "Refresh server:{0} Successfull".format(sid)
            else:
                status = False
                print "Refresh server:{0} Failed".format(sid)
        if status:
            print "Refresh all servers Successfull"
            return
    print "Refresh all servers Failed"


args = parser.parse_args()
lunids = args.lunid
vmnames = args.vmname

if args.command == "mapdisk":
    slot = args.slot
    diskname = args.diskname
    mapdisk(lunids, vmnames, slot, diskname)

if args.command == "unmapdisk":
    unmapdisk(lunids, vmnames)

if args.command == "deletedisk":
    deletedisk(lunids)

if args.command == "refresh":
    refreshserver()
